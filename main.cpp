#include <iostream>
#include <vector>
#include <complex>
#include <mpi.h>
#include <mkl_cdft.h>
#include <array>
#include <fstream>

using complexS = std::complex<float>;
using complexD = std::complex<double>;

static constexpr std::size_t nx = 16384, ny = 3;

static const MKL_LONG dim_sizes[] = { nx, ny };

DFTI_DESCRIPTOR_DM_HANDLE desc = nullptr;

static std::array<complexD, nx * ny> globalIN; // Static array, not dynamic
static std::array<complexD, nx * ny> globalOUT; // Static array, not dynamic

static std::vector<complexD> in;

static std::vector<complexD> out;

static MKL_LONG status, v, n, s;


int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	int rank_tmp, world_size_tmp;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank_tmp);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size_tmp);
	const int rank = rank_tmp, world_size = world_size_tmp;

	if (argc != 2) {
		if (rank == 0) {
			std::cerr << "Fichier pour le temps." << std::endl;
		}
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	const double start = MPI_Wtime();
	//Ligne 0, colonne 0 = 0
	globalIN[ny * 0 + 1] = 3.6;
	globalIN[ny * 0 + 2] = 2.6;

	globalIN[ny * 1 + 0] = 1;
	globalIN[ny * 1 + 1] = 2.9;
	globalIN[ny * 1 + 2] = 6.3;

	globalIN[ny * 2 + 0] = 2;
	globalIN[ny * 2 + 1] = 5.6;
	globalIN[ny * 2 + 2] = 4.0;

	globalIN[ny * 3 + 0] = 3;
	globalIN[ny * 3 + 1] = 4.8;
	globalIN[ny * 3 + 2] = 9.1;

	globalIN[ny * 4 + 0] = 4;
	globalIN[ny * 4 + 1] = 3.3;
	globalIN[ny * 4 + 2] = 0.4;

	globalIN[ny * 5 + 0] = 5;
	globalIN[ny * 5 + 1] = 5.9;
	globalIN[ny * 5 + 2] = 4.8;

	globalIN[ny * 6 + 0] = 6;
	globalIN[ny * 6 + 1] = 5.0;
	globalIN[ny * 6 + 2] = 2.6;

	globalIN[ny * 7 + 0] = 7;
	globalIN[ny * 7 + 1] = 4.3;
	globalIN[ny * 7 + 2] = 4.1;

	// Le compilo a intérêt à dérouler la boucle…
	for (size_t i = 8; i < nx; ++i) {
		globalIN[ny * i + 0] = i;
	}

	status = DftiCreateDescriptorDM(MPI_COMM_WORLD, &desc, DFTI_DOUBLE, DFTI_COMPLEX, 2, dim_sizes); //Specify size and precision
	status = DftiGetValueDM(desc, CDFT_LOCAL_SIZE, &v);

	in.resize(v), out.resize(v);

	status = DftiGetValueDM(desc, CDFT_LOCAL_NX, &n);
	status = DftiGetValueDM(desc, CDFT_LOCAL_X_START, &s);

	for (size_t i = 0; i < n; ++i) {
		const std::size_t globalRow = i + s;
		for (size_t j = 0; j < ny; ++j) {
			in[i * ny + j] = globalIN[globalRow * ny + j];
		}
	}

	status = DftiSetValueDM(desc, DFTI_PLACEMENT, DFTI_NOT_INPLACE); //Out of place FFT
	status = DftiCommitDescriptorDM(desc); //Finalize the desc
	status = DftiComputeForwardDM(desc, in.data(), out.data()); //Compute the Forward FFT
	status = DftiFreeDescriptorDM(&desc); //Free the desc
	for (size_t i = 0; i < n; ++i) {
		const std::size_t globalRow = i + s;
		for (size_t j = 0; j < ny; ++j) {
			globalOUT[globalRow * ny + j] = out[i * ny + j];
		}
	}
	if (rank == 0) {
		MPI_Gather(out.data(), v, MPI_DOUBLE_COMPLEX, globalOUT.data(), v, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	} else {
		MPI_Gather(out.data(), v, MPI_DOUBLE_COMPLEX, nullptr, v, MPI_DOUBLE_COMPLEX, 0, MPI_COMM_WORLD);
	}
	const double stop = MPI_Wtime();
	if (rank == 0) {
		std::ofstream file(argv[1], std::ios::out | std::ios::app);
		file << stop - start << '\n';
	}
	MPI_Finalize();
	return EXIT_SUCCESS;
}
