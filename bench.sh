# Options SBATCH :
#SBATCH --job-name=FFT_mpi
#SBATCH --ntasks=16
#SBATCH --ntasks-per-node=10
#SBATCH -N 4
#SBATCH --partition=instant
# Jobs Steps (étapes du Job) :
srun -N 1 --ntasks-per-node=1 --ntasks=1 ./fft_para 16384/N1_npn1_n1.txt 
srun -N 1 --ntasks-per-node=2 --ntasks=2 ./fft_para 16384/N1_npn2_n2.txt 
srun -N 1 --ntasks-per-node=4 --ntasks=4 ./fft_para 16384/N1_npn4_n4.txt 
srun -N 1 --ntasks-per-node=8 --ntasks=8 ./fft_para 16384/N1_npn8_n8.txt 
srun -N 1 --ntasks-per-node=16 --ntasks=16 ./fft_para 16384/N1_npn16_n16.txt
srun -N 2 --ntasks-per-node=1 --ntasks=2 ./fft_para 16384/N2_npn1_n2.txt 
srun -N 2 --ntasks-per-node=2 --ntasks=4 ./fft_para 16384/N2_npn2_n4.txt 
srun -N 2 --ntasks-per-node=4 --ntasks=8 ./fft_para 16384/N2_npn4_n8.txt
srun -N 2 --ntasks-per-node=8 --ntasks=16 ./fft_para 16384/N2_npn8_n16.txt 
srun -N 3 --ntasks-per-node=1 --ntasks=3 ./fft_para 16384/N3_npn1_n3.txt 
srun -N 3 --ntasks-per-node=2 --ntasks=6 ./fft_para 16384/N3_npn2_n6.txt 
srun -N 3 --ntasks-per-node=4 --ntasks=12 ./fft_para 16384/N3_npn4_n12.txt 
srun -N 4 --ntasks-per-node=1 --ntasks=4 ./fft_para 16384/N4_npn1_n4.txt
srun -N 4 --ntasks-per-node=2 --ntasks=8 ./fft_para 16384/N4_npn2_n8.txt
